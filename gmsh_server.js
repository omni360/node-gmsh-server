var port = 8888; //require('./sys-conf.js').port;
var io = require('socket.io').listen(port);
console.log('gmsh server running at ' + port);
var gmsh_program = 'gmsh';
var exec = require('child_process').exec;
var fs = require('fs');
var path =require('path');
var child;

io.sockets.on('connection', function (socket) {
    socket.on('geo', function(type, arg) {
        if (type === 'filename') {
            socket.__geo_tmp = false;
            socket.__geo_file_name = path.normalize(arg);
        } else if (type === 'string') {
            var now =  new Date().getTime();
            var tmp_in = path.normalize('./tmp/in.' + now + '.geo');
            fs.writeFileSync(tmp_in, arg);
            socket.__geo_tmp = true;
            socket.__geo_file_name = path.normalize(tmp_in);
        }
    });
    // socket.on('mesh', function(str_in, options) {
    //     console.log('in: ' + str_in);
    //     var now =  new Date().getTime();
    //     var tmp_in = path.normalize('./tmp/in.' + now + '.geo');
    //     var tmp_out = path.normalize('./tmp/out.' + now + '.msh');
    //     debugger;
    //     fs.writeFileSync(tmp_in, str_in);
    //     child = exec(
    //         'gmsh ' + options + 
    //             ' ' + tmp_in +
    //             ' -o ' + tmp_out,
    //         function (error, stdout, stderr) {
    //             console.log('stdout: ' + stdout);
    //             console.log('stderr: ' + stderr);
    //             if (error !== null) {
    //                 console.log('exec error: ' + error);
    //             }
    //             socket.emit('gmesh-stdout', stdout);
    //             socket.emit('gmesh-stderr', stderr);
    //             var data = fs.readFile(tmp_out, function(err, data) {
    //                 debugger;
    //                 if (err) {
    //                     console.log('Error: ', err);
    //                     socket.emit('gmesh-stderr', err);
    //                 } else {
    //                     socket.emit('mesh-created', data.toString());
    //                     fs.unlinkSync(tmp_out);
    //                 }
    //                 if (socket.__geo_tmp === true) {
    //                     fs.unlinkSync(tmp_in);
    //                 }
    //             });
    //         }
    //     );
    // });
    socket.on('gmesh', function(options) {
        if (socket.__geo_file_name) {
            var now =  new Date().getTime();
            var tmp_out = path.normalize('./tmp/out.' + now + '.msh');
            child = exec(
                'gmsh ' + options + 
                    ' ' + socket.__geo_file_name +
                    ' -o ' + tmp_out,
                function (error, stdout, stderr) {
                    console.log('stdout: ' + stdout);
                    console.log('stderr: ' + stderr);
                    if (error !== null) {
                        console.log('exec error: ' + error);
                    }
                    socket.emit('gmesh-stdout', stdout);
                    socket.emit('gmesh-stderr', stderr);
                    var data = fs.readFile(tmp_out, function(err, data) {
                        debugger;
                        if (err) {
                            console.log('Error: ', err);
                            socket.emit('gmesh-stderr', err);
                        } else {
                            socket.emit('mesh-created', data.toString());
                            fs.unlinkSync(tmp_out);
                        }
                        if (socket.__geo_tmp === true) {
                            fs.unlinkSync(socket.__geo_file_name);
                        }
                        // fs.unlinkSync(socket.__geo_file_name);
                    });
                }
            );
        }
    });
});